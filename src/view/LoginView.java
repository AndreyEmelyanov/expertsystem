package view;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import java.sql.SQLException;


public class LoginView extends Application
{
    @FXML
    private TextField loginTF;
    @FXML
    private PasswordField passwordField;
    @FXML
    private Label errorLable;
    @FXML
    private Button loggingBtn;
    @FXML
    private Button prepareRegBtn;

    @FXML
    private TextField regLoginTF;
    @FXML
    private TextField regPassTF;
    @FXML
    private TextField regWorkTF;
    @FXML
    private TextField regMailTF;
    @FXML
    private TextField regPhoneTF;
    @FXML
    private Label regErrorLAbel;
    @FXML
    private Button regExpertBtn;
    @FXML
    private ChoiceBox regEducationChoiseBox;

    private static Stage stage;
    private static LoginView instance;

    private static Controller controller = new Controller();

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        stage = primaryStage;
        stage.setTitle("Экспертная система");
        gotoLogin();
        primaryStage.show();
    }

    public LoginView()
    {
        instance = this;
    }

    public static LoginView getInstance()
    {
        return instance;
    }

    static Stage getStage()
    {
        return stage;
    }

    public static Controller getController()
    {
        return controller;
    }

    public void userLogging() throws Exception //admin, admin
    {
        try
        {
            if (Controller.authentication(loginTF.getText().trim(), passwordField.getText().trim()) != null)
            {

                MainView.gotoProfile();
            }

            else
            {
                errorLable.setVisible(true);
            }
        }
        catch (SQLException e)
        {
            errorLable.setText(e.getMessage());
        }
    }

    public static void gotoLogin()
    {
        try
        {
            replaceSceneContent("fxml/LoginView.fxml");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static Parent replaceSceneContent(String fxml) throws Exception
    {
        Parent page = (Parent) FXMLLoader.load(LoginView.class.getResource(fxml), null, new JavaFXBuilderFactory());
        Scene scene = stage.getScene();
        if (scene == null)
        {
            scene = new Scene(page, 431, 387);
            stage.setScene(scene);
        }
        else
        {
            stage.getScene().setRoot(page);
        }
        stage.sizeToScene();
        stage.resizableProperty().setValue(Boolean.FALSE);
        return page;
    }

    public void prepareRegExpert()
    {
        loginTF.setVisible(false);
        passwordField.setVisible(false);
        loggingBtn.setVisible(false);
        prepareRegBtn.setVisible(false);
        errorLable.setVisible(false);

        regLoginTF.setVisible(true);
        regPassTF.setVisible(true);

        regEducationChoiseBox.getItems().addAll("Общее", "Среднее профессиональное", "Бакалавр",
                "Специалист", "Магистр", "Аспирант", "Доктор Наук", "Академик РАН");

        regEducationChoiseBox.setVisible(true);

        regWorkTF.setVisible(true);
        regMailTF.setVisible(true);
        regPhoneTF.setVisible(true);
        regExpertBtn.setVisible(true);
    }

    public void regExpert()
    {
        try
        {
            Controller.addNewExpert(regLoginTF.getText().trim(), regPassTF.getText().trim(), (String) regEducationChoiseBox.getValue(), regWorkTF.getText().trim(), regMailTF.getText().trim(), regPhoneTF.getText().trim());

            loginTF.setVisible(true);
            passwordField.setVisible(true);
            loggingBtn.setVisible(true);
            prepareRegBtn.setVisible(true);
            errorLable.setVisible(false);

            regLoginTF.setVisible(false);
            regPassTF.setVisible(false);
            regEducationChoiseBox.setVisible(false);
            regWorkTF.setVisible(false);
            regMailTF.setVisible(false);
            regPhoneTF.setVisible(false);
            regExpertBtn.setVisible(false);

        }
        catch (SQLException e)
        {
            regErrorLAbel.setVisible(true);
        }
    }
}
