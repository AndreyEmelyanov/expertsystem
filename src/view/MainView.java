package view;


import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Alternative;
import model.Expert;
import model.InformationTable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

public class MainView
{
    @FXML
    private TabPane mainTabPane;

    @FXML
    private AnchorPane tab2AnchPane;

    ////////////////////////////////////////////
    //tab 0
    @FXML
    private TextField loginTF;
    @FXML
    private TextField passTF;
    @FXML
    private TextField workTF;
    @FXML
    private TextField mailTF;
    @FXML
    private TextField phoneTF;
    @FXML
    private Label errorLabel;
    @FXML
    private ChoiceBox educationChoiseBox;

    ////////////////////////////////////////////
    //tab 1
    @FXML
    private TableView<InformationTable> InformationTableView;
    @FXML
    private TableColumn informationIdCol;
    @FXML
    private TableColumn informationNameCol;
    @FXML
    private TableColumn informationDescriptionCol;
    @FXML
    private TableColumn informationDateCol;
    @FXML
    private TableColumn informationStageUserCol;
    @FXML
    private TableColumn informationCincordCol;
    @FXML
    private TableColumn informationStageExpertsCol;

    @FXML
    private Button saveSurveyBtn;

    ////////////////////////////////////////////
    //tab 2
    @FXML
    private Label notAdminMessage;

    @FXML
    private TextField surveyNameTF;

    @FXML
    private TextArea descriptionSurveyTA;

    @FXML
    private Label tab2Label1;
    @FXML
    private Label tab2Label2;
    @FXML
    private Label tab2Label3;
    @FXML
    private Label tab2Label4;
    @FXML
    private Label tab2Label5;

    @FXML
    private TableView<Expert> allExpertTable;
    @FXML
    private TableColumn allExpertLoginCol;
    @FXML
    private TableColumn allExpertEducationCol;
    @FXML
    private TableColumn allExpertWorkPlaceCol;
    @FXML
    private TableColumn allExpertEmailCol;
    @FXML
    private TableColumn allExpertPhoneCol;

    @FXML
    private Button addExpertSurveyBtn;
    @FXML
    private Button delExpertSurveyBtn;

    @FXML
    private TableView<Expert> surveyExpertTable;
    @FXML
    private TableColumn surveyLoginCol;
    @FXML
    private TableColumn surveyEducationCol;
    @FXML
    private TableColumn surveyWorkPlaceCol;
    @FXML
    private TableColumn surveyEmailCol;
    @FXML
    private TableColumn surveyPhoneCol;

    @FXML
    private TableView<Alternative> altTable;
    @FXML
    private TableColumn altNameCol;
    @FXML
    private TableColumn altDescriptionCol;

    @FXML
    private Button addAltBtn;
    @FXML
    private Button delAltBtn;

    @FXML
    private Button createSurveyBtn;

    ////////////////////////////////////////////
    //tab 3
    @FXML
    private TableView<InformationTable> InformationTableView2;
    @FXML
    private TableColumn informationIdCol2;
    @FXML
    private TableColumn informationNameCol2;
    @FXML
    private TableColumn informationDescriptionCol2;
    @FXML
    private TableColumn informationDateCol2;
    @FXML
    private TableColumn informationCincordCol2;
    @FXML
    private TableColumn informationStageExpertsCol2;

    @FXML
    private TableView<Alternative> altTable2Tab3;
    @FXML
    private TableColumn table1Tab3AltNameCol;
    @FXML
    private TableColumn table1Tab3AltDescCol;
    @FXML
    private TableColumn table1Tab3AltImportanceCol;

    @FXML
    private TableView<Alternative> altTable1Tab3;
    @FXML
    private TableColumn table2Tab3AltNameCol;
    @FXML
    private TableColumn table2Tab3AltDescCol;
    @FXML
    private TableColumn table2Tab3AltImportanceCol;

    @FXML
    private Label tab3CoefLabel;
    ////////////////////////////////////////////////////////////////

    private static Controller controller = LoginView.getController();

    private ObservableList<InformationTable> surveyForUser;
    private ObservableList<InformationTable> surveyForAdmin;
    private ObservableList<Alternative> alternatives;
    private ObservableList<Alternative> altTable1List;
    private ObservableList<Alternative> altTable2List;
    private ObservableList<Expert> surveyExperts;
    private ObservableList<Expert> allExperts;

    private GridPane gridPane;

    private TextField[][] textFields;

    private static Stage stage;

    private int alternativesCount = 1;

    private int surveyId;

    private double altSumTab3 = 0;

    private boolean choiseBoxFlag = false;
    private boolean tab3Flag = false;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //    fxml part
    public static void gotoProfile()
    {
        try
        {
            stage = LoginView.getStage();
            stage.setTitle("Экспертная система");
            stage.setWidth(1148);
            stage.setHeight(700);
            replaceSceneContent("fxml/MainView.fxml");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private static Parent replaceSceneContent(String fxml) throws Exception
    {
        Parent page = (Parent) FXMLLoader.load(LoginView.class.getResource(fxml), null, new JavaFXBuilderFactory());
        Scene scene = stage.getScene();
        if (scene == null)
        {
            scene = new Scene(page, 1148, 700);
            stage.setScene(scene);
        }
        else
        {
            stage.getScene().setRoot(page);
        }
        return page;
    }

    ////////////////////////////////////////////
    // all tab
    public void selectionAdmin() throws SQLException
    {
        if (mainTabPane.getSelectionModel().isSelected(1))
        {
            if (surveyForUser == null)
            {
                surveyForUser = FXCollections.observableArrayList();
                informationIdCol.setCellValueFactory(new PropertyValueFactory<InformationTable, Integer>("id"));
                informationNameCol.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("name"));
                informationDescriptionCol.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("description"));
                informationDateCol.setCellValueFactory(new PropertyValueFactory<InformationTable, Timestamp>("date"));
                informationStageUserCol.setCellValueFactory(new PropertyValueFactory<InformationTable, Boolean>("stageUser"));
                informationCincordCol.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("concordance"));
                informationStageExpertsCol.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("stageExperts"));
                InformationTableView.setItems(surveyForUser);
                InformationTableView.setPlaceholder(new Label("Не найдено актуальных опросов для Вас"));
            }
            surveyForUser = Controller.getSurveyForExpert(Controller.getCurrentExpert().getId());
            InformationTableView.setItems(surveyForUser);
        }
        if (mainTabPane.getSelectionModel().isSelected(2))
        {
            if (Controller.getCurrentExpert().isAdmin())
            {
                allExperts = Controller.getAllExpert();
                allExpertLoginCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("login"));
                allExpertEducationCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("education"));
                allExpertWorkPlaceCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("workPlace"));
                allExpertEmailCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("email"));
                allExpertPhoneCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("phone"));
                allExpertTable.setItems(allExperts);


                if (alternatives == null)
                {
                    Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>()
                    {
                        public TableCell call(TableColumn p)
                        {
                            return new EditingCell();
                        }
                    };
                    alternatives = FXCollections.observableArrayList();

                    altNameCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("name"));
                    altNameCol.setCellFactory(cellFactory);
                    altNameCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Alternative, String>>()
                    {
                        @Override
                        public void handle(TableColumn.CellEditEvent<Alternative, String> t)
                        {
                            ((Alternative) t.getTableView().getItems().get(t.getTablePosition().getRow())).setName(t.getNewValue());
                        }
                    });
                    altDescriptionCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("description"));
                    altDescriptionCol.setCellFactory(cellFactory);
                    altDescriptionCol.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Alternative, String>>()
                    {
                        @Override
                        public void handle(TableColumn.CellEditEvent<Alternative, String> t)
                        {
                            ((Alternative) t.getTableView().getItems().get(t.getTablePosition().getRow())).setDescription(t.getNewValue());
                        }
                    });
                    altTable.setItems(alternatives);
                    altTable.setEditable(true);
                    altTable.setPlaceholder(new Label("Не добавлено ни одной альтернативы"));

                    surveyExperts = FXCollections.observableArrayList();
                    surveyLoginCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("login"));
                    surveyEducationCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("education"));
                    surveyWorkPlaceCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("workPlace"));
                    surveyEmailCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("email"));
                    surveyPhoneCol.setCellValueFactory(new PropertyValueFactory<Expert, String>("phone"));
                    surveyExpertTable.setItems(surveyExperts);
                    surveyExpertTable.setPlaceholder(new Label("Не выбра ни один эксперт"));
                }
            }
            else
            {
                notAdminMessage.setVisible(true);

                tab2Label1.setVisible(false);
                tab2Label2.setVisible(false);
                tab2Label3.setVisible(false);
                tab2Label4.setVisible(false);
                tab2Label5.setVisible(false);

                surveyNameTF.setVisible(false);
                descriptionSurveyTA.setVisible(false);

                addExpertSurveyBtn.setVisible(false);
                delExpertSurveyBtn.setVisible(false);
                addAltBtn.setVisible(false);
                delAltBtn.setVisible(false);
                createSurveyBtn.setVisible(false);

                allExpertTable.setVisible(false);
                surveyExpertTable.setVisible(false);
                altTable.setVisible(false);
            }
        }

        if (mainTabPane.getSelectionModel().isSelected(3))
        {
            if (!tab3Flag)
            {
                altTable1List = FXCollections.observableArrayList();
                altTable2List = FXCollections.observableArrayList();
                InformationTableView2.getSelectionModel().selectedItemProperty().addListener(new ChangeListener()
                {
                    @Override
                    public void changed(ObservableValue observableValue, Object oldValue, Object newValue)
                    {
                        if (InformationTableView2.getSelectionModel().getSelectedItem() != null)
                        {
                            try
                            {
                                altTable2List.clear();
                                altTable1List = Controller.getAlternativeByIdSurvey(InformationTableView2.getSelectionModel().getSelectedItem().getId());
                                altTable1Tab3.setItems(altTable1List);
                            }
                            catch (SQLException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    }
                });


                table1Tab3AltNameCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("name"));
                table1Tab3AltDescCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("description"));
                table1Tab3AltImportanceCol.setCellValueFactory(new PropertyValueFactory<Alternative, Double>("importance"));
                altTable1Tab3.setItems(altTable1List);
                altTable1Tab3.setPlaceholder(new Label("Выберете опрос"));

                table2Tab3AltNameCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("name"));
                table2Tab3AltDescCol.setCellValueFactory(new PropertyValueFactory<Alternative, String>("description"));
                table2Tab3AltImportanceCol.setCellValueFactory(new PropertyValueFactory<Alternative, Double>("importance"));
                altTable2Tab3.setItems(altTable2List);
                altTable2Tab3.setPlaceholder(new Label("Добавьте альтернативы"));

                tab3Flag = true;
            }
            if (surveyForAdmin == null)
            {
                if (Controller.getCurrentExpert().isAdmin())
                {
                    surveyForAdmin = Controller.getSurveyForExpert(-42);
                }
                else
                {
                    surveyForAdmin = Controller.getSurveyForExpert(Controller.getCurrentExpert().getId());
                }
                informationIdCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, Integer>("id"));
                informationNameCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("name"));
                informationDescriptionCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("description"));
                informationDateCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, Timestamp>("date"));
                informationCincordCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("concordance"));
                informationStageExpertsCol2.setCellValueFactory(new PropertyValueFactory<InformationTable, String>("stageExperts"));
                InformationTableView2.setItems(surveyForAdmin);
                InformationTableView2.setPlaceholder(new Label("Не найдено актуальных опросов"));
            }
            else
            {
                if (Controller.getCurrentExpert().isAdmin())
                {
                    surveyForAdmin = Controller.getSurveyForExpert(-42);
                }
                else
                {
                    surveyForAdmin = Controller.getSurveyForExpert(Controller.getCurrentExpert().getId());
                }
                InformationTableView.setItems(surveyForUser);
            }
        }
    }

    ////////////////////////////////////////////
    //tab 0
    public void updateUserData() throws SQLException
    {
        try
        {
            //TODO test
            /*if (controller == null)
                LoginView.getController();*/
            Controller.getExpertById(Controller.getCurrentExpert().getId());
            if (!choiseBoxFlag)
            {
                educationChoiseBox.getItems().add("Общее");
                educationChoiseBox.getItems().add("Среднее профессиональное");
                educationChoiseBox.getItems().add("Бакалавр");
                educationChoiseBox.getItems().add("Специалист");
                educationChoiseBox.getItems().add("Магистр");
                educationChoiseBox.getItems().add("Аспирант");
                educationChoiseBox.getItems().add("Доктор Наук");
                educationChoiseBox.getItems().add("Академик РАН");
                choiseBoxFlag = true;
            }
            loginTF.setText(Controller.getCurrentExpert().getLogin());
            passTF.setText(Controller.getCurrentExpert().getPassword());
            educationChoiseBox.setValue(Controller.getCurrentExpert().getEducation());
            workTF.setText(Controller.getCurrentExpert().getWorkPlace());
            mailTF.setText(Controller.getCurrentExpert().getEmail());
            phoneTF.setText(Controller.getCurrentExpert().getPhone());
        }
        catch (SQLException e)
        {
            errorLabel.setVisible(true);
        }
    }

    public void saveUserData()
    {
        try
        {
            Controller.updateExpertData(loginTF.getText().trim(), passTF.getText().trim(), (String) educationChoiseBox.getValue(), workTF.getText().trim(), mailTF.getText().trim(), phoneTF.getText().trim());
        }
        catch (SQLException e)
        {
            errorLabel.setVisible(true);
        }
    }

    public void logout()
    {
        if (surveyForUser != null)
            surveyForUser.clear();
        if (surveyForAdmin != null)
            surveyForAdmin.clear();
        if (alternatives != null)
            alternatives.clear();
        if (altTable1List != null)
            altTable1List.clear();
        if (altTable2List != null)
            altTable2List.clear();
        if (surveyExperts != null)
            surveyExperts.clear();
        if (allExperts != null)
            allExperts.clear();

        if (tab2AnchPane.getChildren().contains(gridPane))
        {
            tab2AnchPane.getChildren().remove(gridPane);
            saveSurveyBtn.setVisible(false);
        }
        textFields = null;
        alternativesCount = 1;
        surveyId = 0;
        altSumTab3 = 0;
        choiseBoxFlag = false;
        tab3Flag = false;

        Controller.getCurrentExpert().setAdmin(false);
        Controller.getCurrentExpert().setId(0);
        Controller.getCurrentExpert().setLogin("");
        Controller.getCurrentExpert().setPassword("");
        Controller.getCurrentExpert().setWorkPlace("");
        Controller.getCurrentExpert().setEmail("");
        Controller.getCurrentExpert().setPhone("");

        LoginView.gotoLogin();

    }

    ////////////////////////////////////////////
    //tab 1
    public void updateInformationTable() throws SQLException
    {
        surveyForUser = Controller.getSurveyForExpert(Controller.getCurrentExpert().getId());
        InformationTableView.setItems(surveyForUser);
    }

    public void passSurvey() throws SQLException
    {
        if (InformationTableView.getSelectionModel().getSelectedItem() != null)
        {
            if (!InformationTableView.getSelectionModel().getSelectedItem().isStageUser())
            {
                if (gridPane != null)
                {
                    tab2AnchPane.getChildren().remove(gridPane);
                }
                surveyId = InformationTableView.getSelectionModel().getSelectedItem().getId();
                ObservableList<Alternative> alternativesList = Controller.getAlternativeByIdSurvey(surveyId);

                textFields = new TextField[alternativesList.size()][alternativesList.size()];

                gridPane = new GridPane();
                gridPane.setGridLinesVisible(true);

                gridPane.add(new Label(" Критерии между собой "), 0, 0);
                for (int i = 0; i < alternativesList.size(); i++)
                {
                    gridPane.add(new Label(" " + alternativesList.get(i).getName() + " (" + alternativesList.get(i).getDescription() + ") "+ "["+i+"]"), 0, i + 1);
                }

                for (int i = 0; i < alternativesList.size(); i++)
                {
                    gridPane.add(new Label(i+1+""), i + 1, 0);
                }

                TextField tmp;
                for (int i = 1; i < alternativesList.size() + 1; i++)
                {
                    for (int j = 1; j < alternativesList.size() + 1; j++)
                    {
                        tmp = new TextField();
                        textFields[j - 1][i - 1] = tmp;
                        tmp.setPrefWidth(70);
                        tmp.setPrefHeight(25);
                        gridPane.add(tmp, i, j);
                    }
                }

                gridPane.setLayoutY(300);
                gridPane.setLayoutX(10);
                tab2AnchPane.getChildren().add(gridPane);


                saveSurveyBtn.setLayoutY(340 + alternativesList.size() * 25);
                saveSurveyBtn.setVisible(true);
            }
            else
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Ошибка прохождения опроса");
                alert.setHeaderText(null);
                alert.setContentText("Нельзя проходить опрос несколько раз");
                alert.showAndWait();
            }
        }
    }

    public void saveSurvey() throws SQLException
    {
        ArrayList<Double[][]> allMatrix = new ArrayList<>(1);
        Double[][] matrix;
        matrix = new Double[textFields.length][textFields[0].length];
        System.out.println("///////////////////////////////");

        for (int i = 0; i < textFields.length; i++)
        {
            for (int j = 0; j < textFields[0].length; j++)
            {
                try
                {
                    matrix[i][j] = Double.parseDouble(textFields[i][j].getText());
                    System.out.print(" " + textFields[i][j].getText() + " ");
                }
                catch (Exception e)
                {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Ошибка прохождения опроса");
                    alert.setHeaderText(null);
                    alert.setContentText("Некорректный ввод данных, можно использовать только цифры");

                    alert.showAndWait();
                }
            }
            System.out.println();
        }
        allMatrix.add(matrix);

        ArrayList<Double[]> allEigenVectors = Controller.getEigenVectors(allMatrix);

        Controller.addNewResponse(surveyId, Controller.getCurrentExpert().getId(), allEigenVectors.get(0));

        Controller.updateConkordance(surveyId);

        tab2AnchPane.getChildren().remove(gridPane);
        saveSurveyBtn.setVisible(false);

        updateInformationTable();
    }

    ////////////////////////////////////////////
    //tab 2
    public void addExpertSurvey()
    {
        Expert expert = allExpertTable.getSelectionModel().getSelectedItem();
        if (expert != null)
        {
            allExperts.remove(expert);
            surveyExperts.add(expert);
        }
    }

    public void delExpertSurvey()
    {
        Expert expert = surveyExpertTable.getSelectionModel().getSelectedItem();
        if (expert != null)
        {
            surveyExperts.remove(expert);
            allExperts.add(expert);
        }
    }

    public void addAlt()
    {
        if (alternatives.size() == 0)
            alternativesCount = 1;
        alternatives.add(new Alternative("Альтернатива " + alternativesCount, "Описание " + alternativesCount));
        alternativesCount++;
    }

    public void delAlt()
    {
        int row = altTable.getSelectionModel().getSelectedIndex();
        if (row != -1)
            altTable.getItems().remove(row);
    }

    public void createSurvey() throws SQLException
    {
        Controller.addNewSurvey(surveyNameTF.getText().trim(), descriptionSurveyTA.getText().trim(), surveyExperts, alternatives);
        surveyNameTF.setText("");
        descriptionSurveyTA.setText("");
        for (Expert el : surveyExperts)
        {
            allExperts.add(el);
        }
        surveyExperts.clear();
        alternatives.clear();
        mainTabPane.getSelectionModel().select(1);
    }

    ////////////////////////////////////////////
    //tab 3
    public void addAlternative()
    {
        Alternative alt = altTable1Tab3.getSelectionModel().getSelectedItem();
        if (alt != null)
        {
            altTable1List.remove(alt);
            altTable2List.add(alt);

            altSumTab3 += alt.getImportance();

            tab3CoefLabel.setText("Коэффициент: " + new BigDecimal(altSumTab3).setScale(3, RoundingMode.HALF_UP).doubleValue());
        }
    }

    public void delAlternative()
    {
        Alternative alt = altTable2Tab3.getSelectionModel().getSelectedItem();
        if (alt != null)
        {
            altTable2List.remove(alt);
            altTable1List.add(alt);
            altSumTab3 -= alt.getImportance();
            tab3CoefLabel.setText("Коэффициент: " + new BigDecimal(altSumTab3).setScale(3, RoundingMode.HALF_UP).doubleValue());
        }
    }

    //editing altTable in tab 2
    class EditingCell extends TableCell<Alternative, String>
    {
        private TextField textField;

        public EditingCell()
        {
        }

        @Override
        public void startEdit()
        {
            if (!isEmpty())
            {
                super.startEdit();
                createTextField();
                setText(null);
                setGraphic(textField);
                textField.selectAll();
            }
        }

        @Override
        public void cancelEdit()
        {
            super.cancelEdit();

            setText((String) getItem());
            setGraphic(null);
        }

        @Override
        public void updateItem(String item, boolean empty)
        {
            super.updateItem(item, empty);

            if (empty)
            {
                setText(null);
                setGraphic(null);
            }
            else
            {
                if (isEditing())
                {
                    if (textField != null)
                    {
                        textField.setText(getString());
                    }
                    setText(null);
                    setGraphic(textField);
                }
                else
                {
                    setText(getString());
                    setGraphic(null);
                }
            }
        }

        private void createTextField()
        {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.focusedProperty().addListener(new ChangeListener<Boolean>()
            {
                @Override
                public void changed(ObservableValue<? extends Boolean> arg0, Boolean arg1, Boolean arg2)
                {
                    if (!arg2)
                    {
                        commitEdit(textField.getText());
                    }
                }
            });
        }
        private String getString()
        {
            return getItem() == null ? "" : getItem().toString();
        }
    }
}



