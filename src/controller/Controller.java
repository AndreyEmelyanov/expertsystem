package controller;


import javafx.collections.ObservableList;
import model.AHP;
import model.Alternative;
import model.Expert;
import model.InformationTable;

import java.sql.SQLException;
import java.util.ArrayList;

public class Controller
{
    private static Expert currentExpert;

    public Controller()
    {
    }

    public static Expert getCurrentExpert()
    {
        return currentExpert;
    }

    public static Expert authentication(String login, String password) throws SQLException
    {
        currentExpert = ControllerDB.authentication(login, password);
        return currentExpert;
    }

    public static Expert getExpertById(int id) throws SQLException
    {
        currentExpert = ControllerDB.getExpertById(id);
        return currentExpert;
    }

    public static ArrayList<Double[]> getEigenVectors(ArrayList<Double[][]> allMatrix)
    {
        return AHP.getEigenVectors(allMatrix);
    }

    public static Double[] getLambdaVectors(ArrayList<Double[]> allEigenVectors)
    {
        return AHP.getLambdaVectors(allEigenVectors);
    }

    public static void addNewExpert(String login, String password, String education, String workPlace, String email, String phone) throws SQLException
    {
        ControllerDB.addNewExpert(login, password, education, workPlace, email, phone);
    }

    public static void updateExpertData(String login, String password, String education, String workPlace, String email, String phone) throws SQLException
    {
        ControllerDB.updateExpertData(login, password, education, workPlace, email, phone);
    }

    public static ObservableList<Expert> getAllExpert() throws SQLException
    {
        return ControllerDB.getAllExpert();
    }

    public static void addNewSurvey(String name, String description, ObservableList<Expert> experts, ObservableList<Alternative> alternatives) throws SQLException
    {
        ControllerDB.addNewSurvey(name,description,experts, alternatives);
    }

    public static ObservableList<InformationTable> getSurveyForExpert(int expertId) throws SQLException
    {
        return ControllerDB.getSurveyForExpert(expertId);
    }

    public static ObservableList<Alternative> getAlternativeByIdSurvey(int idSurvey) throws SQLException
    {
        return ControllerDB.getAlternativeByIdSurvey(idSurvey);
    }

    public static void addNewResponse(int surveyId, int expertId, Double[] response) throws SQLException
    {
        ControllerDB.addNewResponse(surveyId,expertId,response);
    }

    public static void updateConkordance(int idSurvey) throws SQLException
    {
        ControllerDB.updateConkordance(idSurvey);
    }

    public static double getConcordanceByResponseList(ArrayList<Double[]> responces)
    {
        return AHP.getConcordanceByResponseList(responces);
    }

    public static void updateAlternativesImportance(int surveyId) throws SQLException
    {
        ControllerDB.updateAlternativesImportance(surveyId);
    }

}
