package controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Alternative;
import model.Education;
import model.Expert;
import model.InformationTable;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class ControllerDB
{
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASS = "postgres";


    public static Expert authentication(String login, String password) throws SQLException
    {
        if (password != null && login != null)
        {
            Properties props = new Properties();
            props.setProperty("user", DB_USER);
            props.setProperty("password", DB_PASS);
            Connection conn = DriverManager.getConnection(URL, props);

            String sql = "SELECT * FROM expertsystem.expert WHERE login=? AND password=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            Expert expert = new Expert();

            if (rs.next())
            {
                expert.setId(rs.getInt("id"));
                expert.setEducation(Education.getEducationNameById(rs.getInt("education_id")));
                expert.setLogin(rs.getString("login"));
                expert.setPassword(rs.getString("password"));
                expert.setWorkPlace(rs.getString("work_place"));
                expert.setEmail(rs.getString("email"));
                expert.setPhone(rs.getString("phone"));
                expert.setAdmin(rs.getBoolean("id_admin"));
            }
            else
            {
                expert = null;
            }
            rs.close();
            ps.close();
            conn.close();

            return expert;
        }
        else
            return null;
    }

    public static void addNewExpert(String login, String password, String education, String workPlace, String email, String phone) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);
        String sql = "INSERT INTO expertsystem.expert VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, getIdForNewTableElement("expert"));
        ps.setString(2, login);
        ps.setString(3, password);
        ps.setInt(4, Education.getEducationIdByName(education));
        ps.setString(5, workPlace);
        ps.setString(6, email);
        ps.setString(7, phone);
        ps.setBoolean(8, false);
        ps.executeUpdate();
    }

    public static int getIdForNewTableElement(String tableName) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);
        String sql = "SELECT COUNT(*) FROM expertsystem." + tableName;
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        int result = 0;
        if (rs.next())
        {
            result = (int) rs.getFloat(1);
        }
        return result + 1;
    }

    public static void updateExpertData(String login, String password, String education, String workPlace, String email, String phone) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);
        String sql = "UPDATE expertsystem.expert SET login = ?, password = ?,education_id = ?,work_place = ?,email = ?,phone = ? WHERE id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, login);
        ps.setString(2, password);
        ps.setInt(3, Education.getEducationIdByName(education));
        ps.setString(4, workPlace);
        ps.setString(5, email);
        ps.setString(6, phone);
        ps.setInt(7, Controller.getCurrentExpert().getId());
        ps.executeUpdate();
    }

    public static Expert getExpertById(int id) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "SELECT * FROM expertsystem.expert WHERE id=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Expert expert = new Expert();

        if (rs.next())
        {
            expert.setId(rs.getInt("id"));
            expert.setEducation(Education.getEducationNameById(rs.getInt("education_id")));
            expert.setLogin(rs.getString("login"));
            expert.setPassword(rs.getString("password"));
            expert.setWorkPlace(rs.getString("work_place"));
            expert.setEmail(rs.getString("email"));
            expert.setPhone(rs.getString("phone"));
            expert.setAdmin(rs.getBoolean("id_admin"));
        }
        else
        {
            expert = null;
        }
        rs.close();
        ps.close();
        conn.close();

        return expert;
    }

    public static ObservableList<Expert> getAllExpert() throws SQLException
    {
        ObservableList<Expert> experts = FXCollections.observableArrayList();
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "SELECT * FROM expertsystem.expert";
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Expert expert;

        while (rs.next())
        {
            expert = new Expert();
            expert.setId(rs.getInt("id"));
            expert.setEducation(Education.getEducationNameById(rs.getInt("education_id")));
            expert.setLogin(rs.getString("login"));
            expert.setPassword(rs.getString("password"));
            expert.setWorkPlace(rs.getString("work_place"));
            expert.setEmail(rs.getString("email"));
            expert.setPhone(rs.getString("phone"));
            expert.setAdmin(rs.getBoolean("id_admin"));
            experts.add(expert);
        }
        rs.close();
        ps.close();
        conn.close();
        return experts;
    }

    public static void addNewSurvey(String name, String description, ObservableList<Expert> experts, ObservableList<Alternative> alternatives) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);
        String sql = "INSERT INTO expertsystem.survey(id,name,description) VALUES (?,?,?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        int surveyId = getIdForNewTableElement("survey");
        ps.setInt(1, surveyId);
        ps.setString(2, name);
        ps.setString(3, description);
        ps.executeUpdate();

        for (int i = 0; i < alternatives.size(); i++)
        {
            sql = "INSERT INTO expertsystem.alternative(id, id_survey,serial_number,name,description) VALUES (?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, getIdForNewTableElement("alternative"));
            ps.setInt(2, surveyId);
            ps.setInt(3, i);
            ps.setString(4, alternatives.get(i).getName());
            ps.setString(5, alternatives.get(i).getDescription());
            ps.executeUpdate();
        }

        for (Expert el : experts)
        {
            sql = "INSERT INTO expertsystem.survey_participation(id, id_expert,id_survey) VALUES (?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, getIdForNewTableElement("survey_participation"));
            ps.setInt(2, el.getId());
            ps.setInt(3, surveyId);
            ps.executeUpdate();
        }
    }

    public static ObservableList<InformationTable> getSurveyForExpert(int expertId) throws SQLException
    {
        ObservableList<InformationTable> surveyForUser = FXCollections.observableArrayList();
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);
        String sql;
        PreparedStatement ps;
        ResultSet rs;
        InformationTable infTable;
        if (expertId != -42)
        {
            sql = "SELECT id_survey, passed FROM expertsystem.survey_participation WHERE id_expert =?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, expertId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                infTable = new InformationTable();
                infTable.setId(rs.getInt("id_survey"));
                infTable.setStageUser(rs.getBoolean("passed"));
                surveyForUser.add(infTable);
            }

            for (InformationTable el : surveyForUser)
            {
                sql = "SELECT * FROM expertsystem.survey WHERE id =?";
                ps = conn.prepareStatement(sql);
                ps.setInt(1, el.getId());
                rs = ps.executeQuery();
                while (rs.next())
                {
                    el.setName(rs.getString("name"));
                    el.setDescription(rs.getString("description"));
                    el.setDate(rs.getTimestamp("date"));
                    el.setConcordance(rs.getDouble("concordance"));
                }
            }
        }
        else
        {
            sql = "SELECT * FROM expertsystem.survey";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next())
            {
                infTable = new InformationTable();
                infTable.setId(rs.getInt("id"));
                infTable.setName(rs.getString("name"));
                infTable.setDescription(rs.getString("description"));
                infTable.setDate(rs.getTimestamp("date"));
                infTable.setConcordance(rs.getDouble("concordance"));
                surveyForUser.add(infTable);
            }
        }

        for (InformationTable el : surveyForUser)
        {
            sql = "SELECT COUNT(*) FROM expertsystem.survey_participation WHERE id_survey = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, el.getId());
            rs = ps.executeQuery();
            int all = 0;
            if (rs.next())
            {
                all = (int) rs.getFloat(1);
            }
            sql = "SELECT COUNT(*) FROM expertsystem.survey_participation WHERE id_survey = ? AND survey_participation.passed = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, el.getId());
            ps.setBoolean(2, true);
            rs = ps.executeQuery();
            if (rs.next())
            {
                el.setStageExperts((int) rs.getFloat(1) + " / " + all);
            }
        }
        rs.close();
        ps.close();
        conn.close();
        return surveyForUser;
    }

    public static ObservableList<Alternative> getAlternativeByIdSurvey(int idSurvey) throws SQLException
    {
        ObservableList<Alternative> alternativesForUser = FXCollections.observableArrayList();
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "SELECT * FROM expertsystem.alternative WHERE id_survey = ? ORDER BY serial_number";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, idSurvey);
        ResultSet rs = ps.executeQuery();
        Alternative alternative;

        while (rs.next())
        {
            alternative = new Alternative();
            alternative.setName(rs.getString("name"));
            alternative.setDescription(rs.getString("description"));
            alternative.setImportance(rs.getDouble("importance"));
            alternativesForUser.add(alternative);
        }
        rs.close();
        ps.close();
        conn.close();

        return alternativesForUser;
    }

    public static void addNewResponse(int surveyId, int expertId, Double[] response) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "UPDATE expertsystem.survey_participation SET passed = TRUE WHERE id_expert = ? AND id_survey = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, expertId);
        ps.setInt(2, surveyId);
        ps.executeUpdate();

        sql = "SELECT * FROM expertsystem.survey_participation WHERE id_expert = ? AND id_survey = ?";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, expertId);
        ps.setInt(2, surveyId);
        ResultSet rs = ps.executeQuery();
        int idSurveyParticipation = 0;
        if (rs.next())
        {
            idSurveyParticipation = rs.getInt("id");
        }


        sql = "INSERT INTO expertsystem.response (id, id_survey_participation,responce) VALUES (?,?,?)";
        ps = conn.prepareStatement(sql);
        ps.setInt(1, getIdForNewTableElement("response"));
        ps.setInt(2, idSurveyParticipation);

        for (int i = 0; i < response.length; i++)
        {
            response[i] = new BigDecimal(response[i]).setScale(3, RoundingMode.HALF_UP).doubleValue();
        }

        Array array = conn.createArrayOf("float8", response);
        ps.setArray(3, array);
        ps.executeUpdate();

        rs.close();
        ps.close();
        conn.close();

        updateAlternativesImportance(surveyId);
    }

    public static void updateConkordance(int idSurvey) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "SELECT * FROM expertsystem.survey_participation WHERE id_survey = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, idSurvey);
        ResultSet rs = ps.executeQuery();

        ArrayList<Integer> idSurveyParticipations = new ArrayList<>();

        while (rs.next())
        {
            idSurveyParticipations.add(rs.getInt("id"));
        }

        ArrayList<Double[]> responces = new ArrayList<>();

        for (Integer el : idSurveyParticipations)
        {
            sql = "SELECT * FROM expertsystem.response WHERE id_survey_participation = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, el);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Array responceArray = rs.getArray(2);
                responces.add((Double[]) responceArray.getArray());
            }

        }

        double concordance = Controller.getConcordanceByResponseList(responces);

        sql = "UPDATE expertsystem.survey SET concordance = ? WHERE id = ?";
        ps = conn.prepareStatement(sql);
        ps.setDouble(1, new BigDecimal(concordance).setScale(3, RoundingMode.HALF_UP).doubleValue());
        ps.setInt(2, idSurvey);
        ps.executeUpdate();

        rs.close();
        ps.close();
        conn.close();
    }

    public static void updateAlternativesImportance(int surveyId) throws SQLException
    {
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASS);
        Connection conn = DriverManager.getConnection(URL, props);

        String sql = "SELECT * FROM expertsystem.survey_participation WHERE id_survey = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, surveyId);
        ResultSet rs = ps.executeQuery();

        ArrayList<Integer> idSurveyParticipations = new ArrayList<>();

        while (rs.next())
        {
            idSurveyParticipations.add(rs.getInt("id"));
        }

        ArrayList<Double[]> responces = new ArrayList<>();

        for (Integer el : idSurveyParticipations)
        {
            sql = "SELECT * FROM expertsystem.response WHERE id_survey_participation = ?";
            ps = conn.prepareStatement(sql);
            ps.setInt(1, el);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Array responceArray = rs.getArray(2);
                responces.add((Double[]) responceArray.getArray());
            }
        }

        double[] result = new double[responces.get(0).length];

        for (int i = 0; i < responces.get(0).length; i++)
        {
            for (Double[] responce : responces)
            {
                result[i] += responce[i];
            }
        }

        for (int i = 0; i < result.length; i++)
        {
            result[i] = result[i] / responces.size();
        }

        for (int i = 0; i < result.length; i++)
        {
            sql = "UPDATE expertsystem.alternative SET importance = ? WHERE id_survey = ? AND serial_number = ?";
            ps = conn.prepareStatement(sql);
            ps.setDouble(1, new BigDecimal(result[i]).setScale(3, RoundingMode.HALF_UP).doubleValue());
            ps.setInt(2, surveyId);
            ps.setInt(3, i);
            ps.executeUpdate();
        }
    }
}
