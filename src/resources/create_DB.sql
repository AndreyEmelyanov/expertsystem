CREATE SCHEMA expertsystem;

CREATE TABLE expertsystem.education
(
    id INTEGER PRIMARY KEY NOT NULL,
    description VARCHAR(30) NOT NULL
);

INSERT INTO expertsystem.education VALUES (0,'Общее');
INSERT INTO expertsystem.education VALUES (1,'Среднее профессиональное');
INSERT INTO expertsystem.education VALUES (2,'Бакалавр');
INSERT INTO expertsystem.education VALUES (3,'Специалист');
INSERT INTO expertsystem.education VALUES (4,'Магистр');
INSERT INTO expertsystem.education VALUES (5,'Аспирант');
INSERT INTO expertsystem.education VALUES (6,'Доктор Наук');
INSERT INTO expertsystem.education VALUES (7,'Академик РАН');

CREATE TABLE expertsystem.expert
(
  id INT PRIMARY KEY NOT NULL,
  login VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  education_id INT,
  work_place VARCHAR(50),
  email VARCHAR(50),
  phone VARCHAR(50),
  id_admin BOOLEAN,
  CONSTRAINT education_id FOREIGN KEY (education_id) REFERENCES expertsystem.education (id)
);
CREATE UNIQUE INDEX expert_login_uindex ON expertsystem.expert (login);

INSERT INTO expertsystem.expert values (1, 'admin', 'admin', 2, 'SSAU', 'andreyem95@mail.ru', '12345678', true);
INSERT INTO expertsystem.expert values (2, 'testlogin', 'testpass', 2, 'qwe', 'qwe@jkhad.com', '123', false);
INSERT INTO expertsystem.expert values (3, 'qwe', 'qwe', 4, 'qwe', 'qwe@qwe.qwe', '123', false);
INSERT INTO expertsystem.expert values (4, 'asd', 'asd', 2, 'asd', 'asd@asd.asd', '123', true);
INSERT INTO expertsystem.expert values (5, 'zxc', 'zxc', 0, 'zxc', 'zxc@zxc.zxc', '123', false);

CREATE TABLE expertsystem.survey
(
    id INT PRIMARY KEY NOT NULL,
    name VARCHAR(50),
    description VARCHAR(100),
    date TIMESTAMP DEFAULT current_timestamp,
    concordance DOUBLE PRECISION
);
CREATE UNIQUE INDEX survey_name_uindex ON expertsystem.survey (name);
CREATE UNIQUE INDEX survey_id_uindex ON expertsystem.survey (id);


CREATE TABLE expertsystem.alternative
(
  id INT PRIMARY KEY NOT NULL,
  id_survey INT NOT NULL,
  serial_number INT,
  name VARCHAR(50),
  description VARCHAR(100),
  importance DOUBLE PRECISION,
  CONSTRAINT id_survey FOREIGN KEY (id_survey) REFERENCES expertsystem.survey (id)
);

CREATE TABLE expertsystem.survey_participation
(
  id INT PRIMARY KEY NOT NULL,
  id_expert INT NOT NULL,
  id_survey INT NOT NULL,
  passed BOOLEAN,
  CONSTRAINT id_expert FOREIGN KEY (id_expert) REFERENCES expertsystem.expert (id),
  CONSTRAINT id_survey FOREIGN KEY (id_survey) REFERENCES expertsystem.survey (id)
);
CREATE UNIQUE INDEX survey_participation_id_uindex ON expertsystem.survey_participation (id);


CREATE TABLE expertsystem.response
(
  id INT PRIMARY KEY NOT NULL,
  responce DOUBLE PRECISION[],
  id_survey_participation INT,
  CONSTRAINT id_survey_participation FOREIGN KEY (id_survey_participation) REFERENCES expertsystem.survey_participation (id)
);
CREATE UNIQUE INDEX response_id_uindex ON expertsystem.response (id);

