package model;



public class Expert
{
    private int id;
    private String login;
    private String password;
    private String education;
    private String workPlace;
    private String email;
    private String phone;
    private boolean isAdmin;

    public Expert()
    {
    }

    public Expert(boolean isAdmin, int id, String login, String password, String education, String workPlace, String email, String phone)
    {
        this.isAdmin = isAdmin;
        this.id = id;
        this.login = login;
        this.password = password;
        this.education = education;
        this.workPlace = workPlace;
        this.email = email;
        this.phone = phone;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getEducation()
    {
        return education;
    }

    public void setEducation(String education)
    {
        this.education = education;
    }

    public String getWorkPlace()
    {
        return workPlace;
    }

    public void setWorkPlace(String workPlace)
    {
        this.workPlace = workPlace;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public boolean isAdmin()
    {
        return isAdmin;
    }

    public void setAdmin(boolean admin)
    {
        isAdmin = admin;
    }
}
