package model;


import java.util.ArrayList;

public class Education
{
    private static ArrayList<String> educationName;

    public static String getEducationNameById(int id)
    {
        if (educationName == null)
        {
            educationName = new ArrayList<>(8);
            educationName.add("Общее");
            educationName.add("Среднее профессиональное");
            educationName.add("Бакалавр");
            educationName.add("Специалист");
            educationName.add("Магистр");
            educationName.add("Аспирант");
            educationName.add("Доктор Наук");
            educationName.add("Академик РАН");
            return educationName.get(id);
        }
        else
            return educationName.get(id);
    }

    public static int getEducationIdByName(String name)
    {
        if (educationName == null)
        {
            educationName = new ArrayList<>(8);
            educationName.add("Общее");
            educationName.add("Среднее профессиональное");
            educationName.add("Бакалавр");
            educationName.add("Специалист");
            educationName.add("Магистр");
            educationName.add("Аспирант");
            educationName.add("Доктор Наук");
            educationName.add("Академик РАН");
            return educationName.indexOf(name);
        }
        else
            return educationName.indexOf(name);
    }
}
