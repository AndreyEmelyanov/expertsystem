package model;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class AHP
{
    public static ArrayList<Double[]> getEigenVectors(ArrayList<Double[][]> allMatrix)
    {
        Double[][] matrix;
        ArrayList<Double[]> eigenVectors = new ArrayList<>(allMatrix.size());
        Double[] eigVec;

        Double sum;

        System.out.println("///////////////////////////////");
        System.out.println("Eigen Vectors");
        for (Double[][] anAllMatrix : allMatrix)
        {
            matrix = anAllMatrix;
            eigVec = new Double[matrix.length];

            for (int i = 0; i < matrix.length; i++)
            {
                eigVec[i] = (double) 1;
                for (int j = 0; j < matrix[0].length; j++)
                {
                    eigVec[i] *= matrix[i][j];
                    System.out.print(" " + matrix[i][j] + " ");
                }
                System.out.println();

                eigVec[i] = Math.exp(Math.log(eigVec[i]) / matrix[0].length);
            }

            sum = (double) 0;
            for (double el : eigVec)
            {
                sum += el;
            }

            System.out.println("Norm Eigen Vectors");

            for (int i = 0; i < eigVec.length; i++)
            {
                eigVec[i] = eigVec[i] / sum;
                System.out.println(eigVec[i]);
            }

            System.out.println("///////////////////////////////");

            eigenVectors.add(eigVec);
        }
        return eigenVectors;
    }

    public static Double[] getLambdaVectors(ArrayList<Double[]> allEigenVectors) //fist vector - criteria`s
    {
        Double[] criteriaVector = allEigenVectors.get(0);

        Double[] result = new Double[allEigenVectors.size() - 1];
        Arrays.fill(result, (double) 0);

        for (int j = 0; j < result.length; j++)
        {
            for (int i = 0; i < criteriaVector.length; i++)
            {
                result[j] += criteriaVector[i] * allEigenVectors.get(i + 1)[j];

            }
        }
        return result;
    }

    public static double getConcordanceByResponseList(ArrayList<Double[]> responces)
    {

        responces = normalizeList(responces);

        responces = transposition(responces);

        double[] sumRank = new double[responces.size()];
        double temp;
        double medium = 0;

        for (int i = 0; i < responces.size(); i++)
        {
            temp = 0;
            for (int j = 0; j < responces.get(0).length; j++)
            {
                temp += responces.get(i)[j];
            }
            sumRank[i] = temp;
        }

        for (double el : sumRank)
        {
            medium += el;
        }
        medium = medium / sumRank.length;

        for (int i = 0; i < sumRank.length; i++)
        {
            sumRank[i] = (sumRank[i] - medium) * (sumRank[i] - medium);
        }

        double sum = 0;
        for (double el : sumRank)
        {
            sum += el;
        }

        Double result = (12 * sum) / ((responces.get(0).length * responces.get(0).length) * ((responces.size() * responces.size() * responces.size()) - responces.size()));
        return new BigDecimal(result).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static ArrayList<Double[]> normalizeList(ArrayList<Double[]> resonces)
    {
        Double[] el;
        ArrayList<Double> newEl;
        for (Double[] resonce : resonces)
        {
            el = resonce;
            newEl = new ArrayList<>(Arrays.asList(el));
            Collections.sort(newEl);
            for (int j = 0; j < newEl.size(); j++)
            {
                el[j] = (double) newEl.indexOf(el[j]) + 1;
            }
        }
        return resonces;
    }

    public static ArrayList<Double[]> transposition(ArrayList<Double[]> input)
    {
        ArrayList<Double[]> result = new ArrayList<>(input.get(0).length);

        Double[] temp;
        for (int i = 0; i < input.get(0).length; i++)
        {
            result.add(new Double[input.size()]);
        }

        for (int i = 0; i < input.size(); i++)
        {
            temp = input.get(i);
            for (int j = 0; j < temp.length; j++)
            {
                result.get(j)[i] = temp[j];
            }
        }
        return result;
    }
}
