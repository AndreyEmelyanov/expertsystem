package model;


import java.sql.Timestamp;

public class InformationTable
{
    private int id;
    private String name;
    private String description;
    private Timestamp date;
    private boolean stageUser;
    private double concordance;
    private String stageExperts;

    public InformationTable()
    {
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Timestamp getDate()
    {
        return date;
    }

    public void setDate(Timestamp date)
    {
        this.date = date;
    }

    public boolean isStageUser()
    {
        return stageUser;
    }

    public void setStageUser(boolean stageUser)
    {
        this.stageUser = stageUser;
    }

    public double getConcordance()
    {
        return concordance;
    }

    public void setConcordance(double concordance)
    {
        this.concordance = concordance;
    }

    public String getStageExperts()
    {
        return stageExperts;
    }

    public void setStageExperts(String stageExperts)
    {
        this.stageExperts = stageExperts;
    }
}
