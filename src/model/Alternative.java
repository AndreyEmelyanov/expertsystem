package model;


public class Alternative
{
    private String name;
    private String description;
    private double importance;

    public Alternative()
    {
    }

    public Alternative(String name, String description, double importance)
    {
        this.name = name;
        this.description = description;
        this.importance = importance;
    }

    public Alternative(String name, String description)
    {
        this.name = name;
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public double getImportance()
    {
        return importance;
    }

    public void setImportance(double importance)
    {
        this.importance = importance;
    }
}
